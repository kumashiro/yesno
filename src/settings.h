/*
 * settings.h  - Settings and argument parsing functions (header file).
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef SETTINGS_H
#define SETTINGS_H

typedef enum {
    ARG_FLAG_NONE = 0,
    ARG_FLAG_HELP = 1,
    ARG_FLAG_VERSION = 2,
    ARG_FLAG_DEFAULT_YES = 4,
    ARG_FLAG_NOECHO = 8,
    ARG_FLAG_FULL_ANSWER = 16,
    ARG_FLAG_OBSESS = 32,
    ARG_FLAG_PRINT = 64,
    ARG_FLAG_AUTO_YES = 128,
    ARG_FLAG_AUTO_NO = 256
} setting_arg_t;


typedef struct {
    setting_arg_t   flags;
    char const      *question;
    long            timeout;
    long            beep;
} Settings;


int  settings_parse_args(int  argc, char * const  *argv, Settings  *settings);
Settings  *settings_new(void);

#endif /* SETTINGS_H */
/* vim: set ft=c sw=4 sts=4 et: */
