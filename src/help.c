/*
 * help.c  - Help functions.
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <stdio.h>

#include "config.h"
#include "gettext.h"
#include "help.h"


void  help_print(FILE  *stream)
{
    fprintf(stream, _(
        "Usage:  %s [ -efhopVy ] [ -a | -A ] [ -b SECS ] [ -t SECS ] [ QUESTION ]\n\n"
        "  -a, --auto-no      answer 'no' automatically\n"
        "  -A, --auto-yes     answer 'yes' automatically\n"
        "  -b, --beep SECS    emit audible sound each last SECS\n"
        "  -e, --noecho       do not print user input\n"
        "  -f, --full         request full \"yes\" or \"no\" answer\n"
        "  -h, --help         print help and exit\n"
        "  -o, --obsess       be obsessive about the correct answer\n"
        "  -p, --print        print result identificator on stderr\n"
        "  -t, --timeout SECS choose default answer after SECS seconds\n"
        "  -V, --version      print versions and exit\n\n"
        "  -y, --yes          set YES as default answer\n"
        "  QUESTION           question to ask\n"),
        PACKAGE_NAME
    );
}


inline
void  help_print_version(FILE  *stream)
{
    fprintf(stream, PACKAGE_NAME " " YESNO_VERSION "\n");
}

/* vim: set ft=c sw=4 sts=4 et: */