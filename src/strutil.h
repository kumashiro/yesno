/*
 * strutil.h  - String functions (header file).
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef STRUTIL_H
#define STRUTIL_H

char  *string_lower(char  *string);
char  *string_strip(char  *string);
int  string2long(char  *string, long  *result, char  **end);
char  *string_time(long  secs, char  *buffer);

#endif /* STRUTIL_H */
/* vim: set ft=c sw=4 sts=4 et: */
