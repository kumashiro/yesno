/*
 * settings.c  - Settings and argument parsing functions.
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "gettext.h"
#include "settings.h"
#include "strutil.h"

#define ARGSPEC     "aAb:efhopt:Vy"


#ifdef HAVE_GETOPT_LONG
static struct option    long_options[] = {
    {"auto-no",       no_argument, NULL, 'a'},
    {"auto-yes",      no_argument, NULL, 'A'},
    {"beep",    required_argument, NULL, 'b'},
    {"help",          no_argument, NULL, 'h'},
    {"full",          no_argument, NULL, 'f'},
    {"obsess",        no_argument, NULL, 'o'},
    {"print",         no_argument, NULL, 'p'},
    {"timeout", required_argument, NULL, 't'},
    {"yes",           no_argument, NULL, 'y'},
    {"version",       no_argument, NULL, 'V'},
    {NULL,                      0, NULL,   0}
};
#endif  /* HAVE_GETOPT_LONG */


int  settings_parse_args(int  argc, char * const  *argv, Settings  *settings)
{
    int     arg;
    char    *endptr;


    do {
#ifdef HAVE_GETOPT_LONG
        arg = getopt_long(argc, argv, ARGSPEC, long_options, NULL);
#else
        arg = getopt(argc, argv, ARGSPEC);
#endif /* HAVE_GETOPT_LONG */
        switch ( arg ) {
            case 'a':
                settings->flags |= ARG_FLAG_AUTO_NO;
                settings->flags &= ~ARG_FLAG_AUTO_YES;
                break;
            case 'A':
                settings->flags |= ARG_FLAG_AUTO_YES;
                settings->flags &= ~ARG_FLAG_AUTO_NO;
                break;
            case 'b':
                if ( string2long(optarg, &(settings->beep), &endptr) != 0 ) {
                    fprintf(stderr, _("Invalid beep value: %s\n"), optarg);
                    return 10;
                } else if ( settings->beep <= 0 || settings->beep > YESNO_MAX_TIMEOUT ) {
                    fprintf(stderr, _("Beep value out of range 1-%li: %li\n"),
                            YESNO_MAX_TIMEOUT, settings->beep);
                    return 10;
                };
                break;
            case 'e':
                settings->flags |= ARG_FLAG_NOECHO;
                break;
            case 'f':
                settings->flags |= ARG_FLAG_FULL_ANSWER;
                break;
            case 'h':
                settings->flags |= ARG_FLAG_HELP;
                return 0;
            case 'o':
                settings->flags |= ARG_FLAG_OBSESS;
                break;
            case 'p':
                settings->flags |= ARG_FLAG_PRINT;
                break;
            case 't':
                if ( string2long(optarg, &(settings->timeout), &endptr) != 0 ) {
                    fprintf(stderr, _("Invalid timeout value: %s\n"), optarg);
                    return 10;
                } else if ( settings->timeout <= 0 || settings->timeout > YESNO_MAX_TIMEOUT ) {
                    fprintf(stderr, _("Timeout value out of range 1-%li: %li\n"),
                            YESNO_MAX_TIMEOUT, settings->timeout);
                    return 10;
                };
                break;
            case 'V':
                settings->flags |= ARG_FLAG_VERSION;
                return 0;
            case 'y':
                settings->flags |= ARG_FLAG_DEFAULT_YES;
                break;
            case '?':
                return 10;
            default:
                break;
        };
    } while ( arg != -1 );

    if ( optind < argc )
        settings->question = argv[optind];

    if ( settings->flags & ARG_FLAG_AUTO_NO || settings->flags & ARG_FLAG_AUTO_YES )
        settings->beep = settings->timeout = -1;

    return 0;
}


Settings  *settings_new(void)
{
    Settings        *settings;


    if ( (settings = (Settings*)malloc(sizeof(Settings))) == NULL )
        abort();

    settings->flags = ARG_FLAG_NONE;
    settings->question = YESNO_DEFAULT_QUESTION;
    settings->timeout = -1;
    settings->beep = -1;

    return settings;
}

/* vim: set ft=c sw=4 sts=4 et: */