/*
 * answer.c  - Functions getting answers from user.
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif /* _GNU_SOURCE */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>

#include "answer.h"
#include "config.h"
#include "gettext.h"
#include "strutil.h"


typedef struct {
    struct termios  ioc_orig;
    struct termios  ioc_new;
} TerminalState;


static
TerminalState  *setup_terminal(Settings  *settings)
{
    TerminalState   *state;


    if ( (state = (TerminalState*)malloc(sizeof(TerminalState))) == NULL )
        abort();

    tcgetattr(STDIN_FILENO, &(state->ioc_orig));
    memcpy(&(state->ioc_new), &(state->ioc_orig), sizeof(struct termios));

    state->ioc_new.c_lflag &= ~ICANON;
    if ( settings->flags & ARG_FLAG_NOECHO && !(settings->flags & ARG_FLAG_FULL_ANSWER) )
        state->ioc_new.c_lflag &= ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &(state->ioc_new));

    return state;
}


static
void  restore_terminal(TerminalState  *state)
{
    tcsetattr(STDIN_FILENO, TCSANOW, &(state->ioc_orig));
    free(state);
}


static
int  wait_for_input(long  secs)
{
    fd_set          rfdset;
    struct timeval  timeout, *tv;
    int             ret;
    char            timestr[12];


    if ( secs >= 0 ) {
        fprintf(stdout, "(%s) ", string_time(secs, timestr));
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
        tv = &timeout;
    } else {
        fprintf(stdout, " ");
        tv = NULL;
    };

    fflush(stdout);

    if ( secs == 0 )
        return 0;

    FD_ZERO(&rfdset);
    FD_SET(STDIN_FILENO, &rfdset);

    ret = select(1, &rfdset, NULL, NULL, tv);
    if ( ret < 0 ) {
        perror("select()");
        return -1;
    };

    return ret;
}


static
void  empty_stdin(void)
{
    char            c;


    do {
        read(0, &c, 1);
    } while ( c != '\n' && c != 4 );
}


static
size_t  read_line(char  *buffer, size_t  buflen)
{
    char            c;
    size_t          len;


    len = 0;
    do {
        if ( !read(0, &c, 1) )
            break;
        buffer[len++] = c;
    } while ( c != '\n' && len < buflen );

    buffer[len] = '\0';
    return len;
}


answer_t  answer_stroke(Settings  *settings)
{
    TerminalState   *state;
    char            input, *ny, *NY;
    long            secs;
    answer_t        answer;
    int             ret;


    /* Lame workaround for translations */
    ny = _("ny");
    NY = _("NY");

    answer = settings->flags & ARG_FLAG_DEFAULT_YES ? ANSWER_YES : ANSWER_NO;
    state = setup_terminal(settings);
    secs = settings->timeout;
    while ( secs >= 0 || settings->timeout == -1 ) {
        if ( settings->beep > 0 && secs <= settings->beep )
            fprintf(stdout, "\a");

        fprintf(stdout, "\r%s [%c/%c]",
                settings->question,
                settings->flags & ARG_FLAG_DEFAULT_YES ? NY[ANSWER_YES] : ny[ANSWER_YES],
                !(settings->flags & ARG_FLAG_DEFAULT_YES) ? NY[ANSWER_NO] : ny[ANSWER_NO]);

        if ( settings->flags & ARG_FLAG_AUTO_NO ) {
            printf(" %c\n", ny[ANSWER_NO]);
            answer = ANSWER_NO;
            break;
        } else if ( settings->flags & ARG_FLAG_AUTO_YES ) {
            printf(" %c\n", ny[ANSWER_YES]);
            answer = ANSWER_YES;
            break;
        };

        if ( (ret = wait_for_input(secs)) < 0 ) {
            answer = ANSWER_ERROR;
            break;
        } else if ( ret == 0 && secs > 0 ) {
            secs--;
            continue;
        } else if ( secs == 0 ) {
            break;
        };

        input = getchar();
        if ( input != '\n' || settings->flags & ARG_FLAG_NOECHO )
            putchar('\n');
        else if ( input == '\n' || input == 4 )
            break;

        if ( settings->flags & ARG_FLAG_OBSESS && index(ny, tolower(input)) == NULL ) {
            fprintf(stderr, _("Please, answer \"y\" or \"n\"\n"));
            continue;
        }

        answer = ( input == NY[ANSWER_YES] || input == ny[ANSWER_YES] );
        break;
    };
    restore_terminal(state);

    if ( secs == 0 )
        fprintf(stdout, "%c\n", settings->flags & ARG_FLAG_DEFAULT_YES ? ny[ANSWER_YES] : ny[ANSWER_NO]);

    return answer;
}


answer_t  answer_full(Settings  *settings)
{
    TerminalState   *state;
    char            input[YESNO_MAX_ANSWER_LENGTH], *stripped;
    size_t          len;
    long            secs;
    answer_t        answer;
    int             ret;


    answer = settings->flags & ARG_FLAG_DEFAULT_YES ? ANSWER_YES : ANSWER_NO;
    state = setup_terminal(settings);
    secs = settings->timeout;
    while ( secs >= 0 || settings->timeout == -1 ) {
        if ( settings->beep > 0 && secs <= settings->beep )
            fprintf(stdout, "\a");

        fprintf(stdout, "\r%s [%s/%s]",
                settings->question,
                settings->flags & ARG_FLAG_DEFAULT_YES ? _("YES") : _("yes"),
                !(settings->flags & ARG_FLAG_DEFAULT_YES) ? _("NO") : _("no"));

        if ( settings->flags & ARG_FLAG_AUTO_NO ) {
            printf(" %s\n", _("no"));
            answer = ANSWER_NO;
            break;
        } else if ( settings->flags & ARG_FLAG_AUTO_YES ) {
            printf(" %s\n", _("yes"));
            answer = ANSWER_YES;
            break;
        };

        if ( (ret = wait_for_input(secs)) < 0 ) {
            answer = ANSWER_ERROR;
            break;
        } else if ( ret == 0 && secs > 0 ) {
            secs--;
            continue;
        } else if ( secs == 0 ) {
            break;
        };

        len = read_line(input, sizeof(answer));
        if ( len == 0 || (len == 1 && *input == '\n') )
            break;
        else if ( input[len - 1] != '\n' )
            empty_stdin();

        stripped = string_strip(string_lower(input));
        if ( strcmp(stripped, _("yes")) == 0 ) {
            answer = ANSWER_YES;
            break;
        } else if ( strcmp(stripped, _("no")) == 0 ) {
            answer = ANSWER_NO;
            break;
        } else {
            fprintf(stderr, _("Please, provide full \"yes\" or \"no\" answer\n"));
            continue;
        };
    };
    restore_terminal(state);

    if ( secs == 0 )
        fprintf(stdout, "%s\n", settings->flags & ARG_FLAG_DEFAULT_YES ? _("yes") : _("no"));

    return answer;
}

/* vim: set ft=c sw=4 sts=4 et: */
