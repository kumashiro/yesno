/*
 * strutil.c  - String functions.
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE       700
#endif /* _XOPEN_SOURCE */

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "strutil.h"


char  *string_lower(char  *string)
{
    char    *c;


    c = string;
    while ( *c != '\0' ) {
        *c = tolower(*c);
        c++;
    };

    return string;
}


char  *string_strip(char  *string)
{
    char    *pos;
    size_t  len;


    len = strlen(string);
    if ( len == 0 )
        return strdup(string);

    /* Insert nul byte at the end of the string */
    pos = string + len;
    while ( len-- ) {
        pos = string + len;
        if ( !isspace(*pos) && *pos != '\n' && *pos != '\0' ) {
            string[len + 1] = '\0';
            break;
        };
    };

    /* Find and return the first non-space position */
    pos = string;
    while ( isspace(*pos) )
        pos++;

    return pos;
}


inline
int  string2long(char  *string, long  *result, char  **end)
{
    *result = strtol(string, end, 10);
    return (*result == LONG_MAX && errno == ERANGE) || *end == string;
}


char  *string_time(long  secs, char  *buffer)
{
    static unsigned char    emap;
    ldiv_t  dres;
    char    *pos;


    pos = buffer;
    dres = ldiv(secs, 86400);
    if ( dres.quot != 0 || emap >= (1 << 2) ) {
        sprintf(pos, "%lid", dres.quot);
        pos += 2;
        emap |= (1 << 2);
    };

    dres = ldiv(dres.rem, 3600);
    if ( dres.quot != 0 || emap >= (1 << 1) ) {
        sprintf(pos, "%02lih", dres.quot);
        pos += 3;
        emap |= (1 << 1);
    };

    dres = ldiv(dres.rem, 60);
    if ( dres.quot != 0 || emap >= (1 << 0) ) {
        sprintf(pos, "%02lim", dres.quot);
        pos += 3;
        emap |= (1 << 0);
    };

    sprintf(pos, "%02lis", dres.rem);
    pos += 3;
    *pos = '\0';
    return buffer;
}

/* vim: set ft=c sw=4 sts=4 et: */
