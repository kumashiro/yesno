/*
 * yesno.c  - Ask a yes/no question.
 *
 * This file is part of yesno project.
 *
 * yesno is free  software: you can redistribute it and/or  modify it under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * yesno is  distributed in the  hope that it will  be useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * yesno. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include "answer.h"
#include "config.h"
#include "gettext.h"
#include "help.h"
#include "settings.h"

static int      print_abort = 0;

static
void  user_abort(int  sig)
{
    (void)sig;
    if ( print_abort != 0 )
        fprintf(stderr, "ABORT\n");
    else
        putchar('\n');
    exit(ANSWER_ABORT);
}


int  main(int  argc, char * const  argv[])
{
    int             code;
    Settings        *settings;


    signal(SIGINT, user_abort);
    signal(SIGTERM, user_abort);

#ifdef SIGKILL
    signal(SIGKILL, user_abort);
#endif /* SIGKILL */
#ifdef SIGQUIT
    signal(SIGQUIT, user_abort);
#endif /* SIGQUIT */

#ifdef GETTEXT
    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE_NAME, LOCALEDIR);
    textdomain(PACKAGE_NAME);
#endif /* GETTEXT */

    settings = settings_new();
    code = settings_parse_args(argc, argv, settings);
    if ( code != 0 || settings->flags & ARG_FLAG_HELP ) {
        help_print(code == 1 ? stderr : stdout);
        free(settings);
        return ANSWER_ERROR;
    } else if ( settings->flags & ARG_FLAG_VERSION ) {
        help_print_version(stdout);
        free(settings);
        return 0;
    };

    print_abort = settings->flags & ARG_FLAG_PRINT;

    if ( settings->flags & ARG_FLAG_FULL_ANSWER )
        code = answer_full(settings);
    else
        code = answer_stroke(settings);

    if ( settings->flags & ARG_FLAG_PRINT ) {
        switch ( code ) {
            case ANSWER_NO:
                fprintf(stderr, "NO\n");
                break;
            case ANSWER_YES:
                fprintf(stderr, "YES\n");
                break;
            case ANSWER_ABORT:
                fprintf(stderr, "ABORT\n");
                break;
            default:
                fprintf(stderr, "ERROR\n");
                break;
        };
    };

    free(settings);
    return code >= 0 ? code : ANSWER_ERROR;
}

/* vim: set filetype=c expandtab tabstop=4 sts=4 shiftwidth=4: */
