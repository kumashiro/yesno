#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.68])
AC_INIT([yesno], [0.9.0], [yesno])
AM_INIT_AUTOMAKE
m4_include([m4/cc_flags.m4])
AC_CONFIG_SRCDIR([src/yesno.c])
AC_CONFIG_HEADERS([src/config.h])
AC_CONFIG_FILES([Makefile
                 Makevars
                 uninstall.sh
                 po/Makefile
                 src/Makefile])
AC_DEFINE([YESNO_VERSION], ["AC_PACKAGE_VERSION"])
AC_DEFINE([PACKAGE_NAME], ["AC_PACKAGE_NAME"])
CFLAGS=""
AX_CC_FLAGS_OPTIONAL([-Wall -Wextra -pedantic -pedantic-errors -pipe])
AX_CC_FLAGS_REQUIRED([-std=c11])

AC_ARG_WITH(gettext, AC_HELP_STRING([--with-gettext=[DIR]], [Support translations using Gettext]),
            gettext=$withval, gettext=yes)

# Checks for programs.
AC_PROG_CC
AC_PROG_INSTALL
AC_PATH_PROG([CHMOD], [chmod])
AC_PATH_PROG([GZIP], [gzip])
AC_PATH_PROG([CP], [cp])
AC_PATH_PROG([RM], [rm])

# Checks for libraries.
AC_CHECK_HEADERS([getopt.h], [
    AC_CHECK_FUNC([getopt_long], [AC_DEFINE([HAVE_GETOPT_LONG])], [
        AC_CHECK_FUNC([getopt], [AC_DEFINE([HAVE_GETOPT])], [
            AC_MSG_ERROR([Required getopt functions were not found.])])])])

if test "$gettext" != "no" ; then
    if test "$gettext" != "yes"; then
        LDFLAGS="${LDFLAGS} -L$gettext/lib"
        CPPFLAGS="${CPPFLAGS} -I$gettext/include"
    elif test -f "/usr/local/lib/libintl.so" ; then
        LDFLAGS="${LDFLAGS} -L/usr/local/lib"
    fi
    AC_CHECK_LIB([intl], [gettext])
    AC_CHECK_HEADER([libintl.h],
        AC_CHECK_FUNC([gettext], [gettext=yes], gettext=no), gettext=no)
fi
if test "x$gettext" != "xno" ; then
    AC_PATH_PROG([XGETTEXT], [xgettext], [nf])
    AC_PATH_PROG([MSGINIT], [msginit], [nf])
    AC_PATH_PROG([MSGMERGE], [msgmerge], [nf])
    AC_PATH_PROG([MSGFMT], [msgfmt], [nf])
    if test "x$XGETTEXT" = "xnf" -o "x$MSGINIT" = "xnf" -o "x$MSGMERGE" = "xnf" -o "x$MSGFMT" = "xnf" ; then
        AC_MSG_WARN([Gettext tools were not found. Translations will be unavailable.])
        AC_DEFINE(GETTEXT, 0, [Define to 1 if you want Gettext support.])
    else
        AC_DEFINE(GETTEXT, 1, [Define to 1 if you want Gettext support.])
        AC_SUBST([LOCALEDEFS], [-DLOCALEDIR=\\\"$localedir\\\"])
        AC_SUBST([CATALOG], [LC_MESSAGES])
    fi
else
    AC_MSG_WARN([Gettext not found or disabled. Translations will be unavailable.])
fi
AC_MSG_CHECKING([if Gettext should be used])
AC_MSG_RESULT($gettext)

AC_MSG_CHECKING([if man page can be compressed])
manfile=`man -w man 2>/dev/null`
if test "x`basename ${manfile}`" = "xman.1.gz" ; then
    AC_SUBST([MANEXT], [.gz])
    AC_MSG_RESULT([yes])
else
    AC_MSG_RESULT([no])
fi

# Checks for header files.
AC_CHECK_HEADERS([limits.h locale.h stdlib.h string.h strings.h termios.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T
AC_C_INLINE

# Checks for library functions.
AC_FUNC_MALLOC
AC_CHECK_FUNCS([select setlocale signal strdup strtol])

AC_OUTPUT
