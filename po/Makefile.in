include ../Makevars
SRCS		:= ../src/$(package).c ../src/help.c ../src/answer.c ../src/settings.c
DESTDIR		:= $(localedir)
XGETTEXT	:= @XGETTEXT@
XGETTEXT_ARGS	?= -d $(package) -C -k_ -kN_ -T -s --foreign-user --package-name=$(package)
MSGINIT		:= @MSGINIT@
MSGINIT_ARGS	?= 
MSGMERGE	:= @MSGMERGE@
MSGMERGE_ARGS	?= -U
MSGFMT		:= @MSGFMT@
MSGFMT_ARGS	?= 
INSTALL		:= @INSTALL@
LINGUAS		?= pl
POTFILE		:= $(package).pot
MOFILES		:= $(patsubst %,%/$(package).mo,$(LINGUAS))
DEST_FILES	:= $(patsubst %,$(DESTDIR)/%/$(CATALOG)/$(package).mo,$(LINGUAS))

.PHONY: all clean install pot squeky-clean

.PRECIOUS: $(POTFILE) $(SRCS) $(MOFILES:.mo=.po)

.FORCE:

all: $(MOFILES)

pot: $(POTFILE)

$(POTFILE):
	$(XGETTEXT) $(XGETTEXT_ARGS) -o $@ $(SRCS)

%.po: $(POTFILE)
	$(INSTALL) -d $(@D)
	if [ -e $@ ]; then \
		$(MSGMERGE) $(MSGMERGE_ARGS) $@ $<; \
	else \
		$(MSGINIT) $(MSGINIT_ARGS) -i $(POTFILE) -o $@ -l $(@D); \
	fi

%.mo: %.po
	$(MSGFMT) $(MSGFMT_ARGS) -o $@ $<

$(DESTDIR)/%/$(CATALOG):
	$(INSTALL) -d $@

$(DESTDIR)/%/$(CATALOG)/$(package).mo: %/$(package).mo $(DESTDIR)/%/$(CATALOG)
	$(INSTALL) $< $@

install: $(DEST_FILES)

clean:
	$(RM) -f $(MOFILES)

squeky-clean: clean
	$(RM) -f Makefile
