# SOME DESCRIPTIVE TITLE.
# This file is put in the public domain.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: yesno\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-12-15 04:19+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/settings.c:59
#, c-format
msgid "Alert value out of range 1-%li: %li\n"
msgstr ""

#: ../src/settings.c:56
#, c-format
msgid "Invalid alert value: %s\n"
msgstr ""

#: ../src/settings.c:81
#, c-format
msgid "Invalid timeout value: %s\n"
msgstr ""

#: ../src/answer.c:222
msgid "NO"
msgstr ""

#: ../src/answer.c:186
#, c-format
msgid "Please, answer \"y\" or \"n\"\n"
msgstr ""

#: ../src/answer.c:248
#, c-format
msgid "Please, provide full \"yes\" or \"no\" answer\n"
msgstr ""

#: ../src/settings.c:84
#, c-format
msgid "Timeout value out of range 1-%li: %li\n"
msgstr ""

#: ../src/help.c:32
#, c-format
msgid ""
"Usage:  %s [ -efhopVy ] [ QUESTION ]\n"
"\n"
"  -e, --noecho      do not print user input\n"
"  -f, --full        request full \"yes\" or \"no\" answer\n"
"  -h, --help        print help and exit\n"
"  -o, --obsess      be obsessive about the correct answer\n"
"  -p, --print       print result identificator on stderr\n"
"  -V, --version     print versions and exit\n"
"\n"
"  -y, --yes         set YES as default answer\n"
"  QUESTION          question to ask\n"
msgstr ""

#: ../src/answer.c:221
msgid "YES"
msgstr ""

#: ../src/answer.c:155
msgid "YN"
msgstr ""

#: ../src/answer.c:222 ../src/answer.c:244 ../src/answer.c:255
msgid "no"
msgstr ""

#: ../src/answer.c:221 ../src/answer.c:241 ../src/answer.c:255
msgid "yes"
msgstr ""

#: ../src/answer.c:154
msgid "yn"
msgstr ""
