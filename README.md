*yesno* is a small program that does one thing: it asks a supplied question and
waits for *yes* or *no* answer. User can reply with single stroke (without the
need to confirm with ENTER key) or may be forced to enter full answer. Default
answer can also be given by pressing ENTER without typying anything else.

Features
========

* single stroke answers or full answers
* localised answers can be defined with standard Gettext mechanism
* emitting default answer after defined timeout
* audible countdown to timeout

Usage
=====

```
yesno [ -efhopVy ] [ -a | -A ] [ -b SECS ] [ -t SECS ] [ QUESTION ]

  -a, --auto-no      answer 'no' automatically
  -A, --auto-yes     answer 'yes' automatically
  -b, --beep SECS    emit audible sound each last SECS
  -e, --noecho       do not print user input
  -f, --full         request full "yes" or "no" answer
  -h, --help         print help and exit
  -o, --obsess       be obsessive about the correct answer
  -p, --print        print result identificator on stderr
  -t, --timeout SECS choose default answer after SECS seconds
  -V, --version      print versions and exit

  -y, --yes          set YES as default answer
  QUESTION           question to ask
```

Example
=======

The following example shows a simple use of `yesno` program.

```sh
yesno "Would you like to take over the world?"
case $? in
    0)
        # User answered NO
        echo "World conquest has been put on hold"
        break
        ;;
    1)
        # User answered YES
        echo "Go and conquer the World!"
        break
        ;;
    2)
        # "yesno" has been killed
        echo "Answer has been aborted"
        break
        ;;
    *)
        # Some error has occured
        echo "Command execution error"
        break
        ;;
esac
```

Compilation and installation
============================

Below is a list of additional software required or recommended for compilation:

  * `required` GNU autotools (aclocal, autoconf, automake)
  * `required` getopt (preferably with `getopt_long()`)
  * `required` gmake
  * `optional` GNU Gettext with tools (xgettext, msginit, msgmerge, msgfmt)

If *getopt* has no `getopt_long()` function, long options will not be available.
If *Gettext* is not present or disabled, error messages translations will not be
available.

To compile `yesno`, clone it and execute the following commands:

```sh
yesno-clone$ ./autoinit.sh
yesno-clone$ ./configure
yesno-clone$ gmake
...
yesno-clone# gmake install
```

You may wish to alter some options before compiling. This can be done by running
`./configure` with arguments. Try `./configure --help` to get a list of
switches.

When recompiling, it is advisable to start the whole process anew. Resetting the
project to its initial state can be done with command:

```sh
yesno-clone$ gmake squeky-clean
```
